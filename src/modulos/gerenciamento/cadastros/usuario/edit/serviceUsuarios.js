﻿angular.module("usuariosservice", [])
.service("serviceUsuarios", ["serviceGeneric",
    function serviceUsuarios(serviceGeneric) {
        const controller = "Usuarios";

        this.VerificarDisponibilidadeLogin = function (user) {
            var params = "";
            return serviceGeneric.callWeb("POST", controller, "verificar-login", params, user);
        };

        this.Save = function(user) {
            var params = "";

            return serviceGeneric.callWeb("POST", controller, "", params, user);
        };
    }]);
angular.module(moduleNames.gerenciamento.usuarios.edit, [])
    .controller("controllerEdit", ["$scope", "serviceUsuarios",
        function controllerEdit($scope, serviceUsuarios) {
            function ResetarVariaveis() {
                $scope.Usuario = new Usuario();
                $scope.LoginVerificado = false;
                $scope.SugestoesLogins = null;
            }

            $scope.VerificarDisponibilidadeLogin = function () {
                serviceUsuarios.VerificarDisponibilidadeLogin($scope.Usuario)
                    .then(function (obj) {
                        if (obj == ""){
                            $scope.SugestoesLogins = null;
                            $scope.LoginVerificado = true;
                        }
                        else
                            $scope.SugestoesLogins = obj;
                    }, function (erro) {
                        console.log(erro);
                    });
            };

            $scope.Salvar = function (form) {
                if (formIsValid(form)) {
                    serviceUsuarios.Save($scope.Usuario)
                        .then(function (obj) {
                            ResetarVariaveis();
                        }, function (erro) {
                            console.log(erro);
                        });
                }
            };

            class Usuario {
                constructor() {
                    this.id = null;
                    this.nome = "";
                    this.nascimento = "";
                    this.email = "";
                    this.login = "";
                    this.senha = "";
                }
            };

            $scope.Usuario = new Usuario();
            $scope.LoginVerificado = false;
            $scope.SugestoesLogins = null;
        }]);
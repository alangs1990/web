const enums = {
    state: {
        inserting: buildEnum("Inserting"),
        editing: buildEnum("Editing"),
    },
    tipoUsuario: {
        estabelecimento: buildEnum("Estabelecimento"),
        motoboy: buildEnum("Motoboy"),
        gerenciamento: buildEnum("Gerenciamento"),
    },
};

function buildEnum(value, display = "") {
    return {
        display: (display != "" ? display : value),
        value
    };
};

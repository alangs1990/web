﻿'use strict';
angular.module("app.services", [])
    .service("serviceGeneric", ["$http", "$q",
        function serviceGeneric($http, $q) {
            this.callWeb = function (methodHttp, controller, action, params, postBody) {
                var deferred = $q.defer();

                var url = "";
                url += baseUrl;
                url += controller;
                url += "/"; 
                
                if (action != "") {
                    url += action;
                    url += "/";
                    url += params;
                }

                var headers = {
                    "Content-Type": "application/json",
                };

                if (postBody === undefined)
                    postBody = null;

                $http({ method: methodHttp, url: url, cache: false, headers: headers, data: postBody }).
                    then(function (response) {
                        var response = response.data;

                        AjustaCamposData(response);

                        deferred.resolve(response);
                    }, function (response) {
                        var data = response.data || 'Request failed';
                        var status = response.status;

                        deferred.reject({ statusCode: status, message: data });
                    });

                return deferred.promise;
            };

            const regexDate = /^\d{4}-\d{2}-\d{2}$/;
            const regexDateTime = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$/;
            const regexDateTime2 = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{2,7}$/;
            
            function AjustaCamposData(response) {
                for (var property in response) {
                    //console.log(property + " = " + response[property]);
                    if (typeof(response[property]) == "string"){
                        if (regexDate.exec(response[property])) {
                            response[property] = new Date(response[property]);
                        } else if (regexDateTime.exec(response[property]) || regexDateTime2.exec(response[property])) {
                            response[property] = new Date(response[property] + "Z");
                        }
                    } else if (typeof(response[property]) == "object") {
                        AjustaCamposData(response[property]);
                    }
                }
            };
        }]);
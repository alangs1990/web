﻿'use strict';
angular.module("routes", ["ui.router", "oc.lazyLoad"])
    .config(["$ocLazyLoadProvider", "$stateProvider", "$urlRouterProvider",
        function ($ocLazyLoadProvider, $stateProvider, $urlRouterProvider) {
            const cache = true;

            const basePath = "modulos/";
            const gerenciamentoPath = `${basePath}gerenciamento/`;

            $urlRouterProvider.otherwise(`/gerenciamento/usuarios`);

            //#region stateProvider
            $stateProvider
                .state(
                    stateNames.gerenciamento.usuarios.edit,
                    buildState(
                        "/gerenciamento/usuarios",
                        `${gerenciamentoPath}cadastros/usuario/edit/edit.html`,
                        [
                            moduleNames.gerenciamento.usuarios.edit,
                        ]
                    )
                )
            //#endregion

            //#region ocLazyLoadProvider
            $ocLazyLoadProvider.config(
                {
                    "debug": false,
                    "events": true,
                    "modules": [
                        buildModule(
                            moduleNames.gerenciamento.usuarios.edit,
                            [
                                `${gerenciamentoPath}cadastros/usuario/edit/edit.js`,
                                `${gerenciamentoPath}cadastros/usuario/edit/serviceUsuarios.js`,
                            ]
                        )
                    ]
                }
            );
            //#endregion

            //#region funções
            function buildModule(name, files) {
                return {
                    name,
                    files,
                    cache
                };
            }

            function buildState(endpoint, templateUrl, resolveModules) {
                return {
                    url: endpoint,
                    views: {
                        "container-view": {
                            templateUrl
                        },
                    },
                    resolve: loadModules(resolveModules)
                };
            }

            function loadModules(resolveModules) {
                return {
                    loadMyCtrl: ["$ocLazyLoad", function ($ocLazyLoad) {
                        return $ocLazyLoad.load(resolveModules);
                    }]
                };
            }
            //#endregion
        }]);
# README #

### Templates utilizados ###

Bootstrap
https://getbootstrap.com/docs/4.6/getting-started/introduction/

Template
https://startbootstrap.com/theme/sb-admin-2

Documentação do GitHub
https://github.com/startbootstrap/startbootstrap-sb-admin-2

Live preview
https://startbootstrap.com/previews/sb-admin-2

### Configurar ###

Após clonar o projeto é necessário instalar os pacotes, use os seguintes comandos para isso:

    npm install -g http-server
    npm install

### Atalhos ###

Registrar os atalhos do VScode:

    - Identar código fonte
        SHIFT + ALT + F
    - Abrir/Fechar o terminal
        CONTROL + J

### Como rodar a aplicação ###

Basta executar o comando abaixo na pasta raiz do projeto:

    npm start
